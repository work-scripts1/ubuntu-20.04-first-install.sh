# add ssh key
ssh-copy-id root@ip

# add user
adduser www && usermod -aG sudo www

# password for sudo - off
sudo visudo
%sudo ALL=(ALL:ALL) NOPASSWD:ALL

ssh-copy-id www@ip

# no root & no pwd
# https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04-ru
sudo vim /etc/ssh/sshd_config
#AllowUsers www
#PermitRootLogin no
#PasswordAuthentication no
sudo systemctl restart sshd

# config firewall 
#https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
sudo ufw allow OpenSSH && sudo ufw allow http && sudo ufw allow https
sudo ufw allow 5000/tcp
sudo ufw allow 60000:61000/udp # for mosh
sudo ufw enable
sudo ufw disable

sudo ufw status

# install mosh (по желанию)
#sudo add-apt-repository ppa:keithw/mosh
#sudo apt-get update
#apt-get install mosh
# reconnect
#mosh www@ip

sudo apt update && sudo apt upgrade -y

# install zsh
sudo apt install zsh -y
# reconnect
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k
vim ~/.zshrc
# ZSH_THEME="powerlevel10k/powerlevel10k"
# alias cls="clear"
source ~/.zshrc


# install Docker
# https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04
ssh www@ip
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
#-y mosh unzip zip gcc build-essential make
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo systemctl status docker
sudo usermod -aG docker ${USER}
# relogin
id -nG
# docker-compose
# https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04
# last reliase https://github.com/docker/compose/releases
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
